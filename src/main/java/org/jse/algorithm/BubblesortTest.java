package org.jse.algorithm;


import java.util.Scanner;


public class BubblesortTest {

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);
		String[] st = sc.next().split(",");

		Integer[] intArray = new Integer[st.length];

		int i = 0;
		for(String s : st){
			intArray[i++] = Integer.parseInt(s);
		}
		

		System.out.print("Before: ");
		for(int k : intArray) {
			System.out.print(k + " ");
		}
		System.out.println("");
		new Bubblesort().sort(intArray);
		System.out.print("After: ");
		for(int k : intArray) {
			System.out.print(k + " ");
		}
		System.out.println("");
	}

}
