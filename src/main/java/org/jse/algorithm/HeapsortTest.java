package org.jse.algorithm;


import java.util.*;


public class HeapsortTest {

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);
		String[] st = sc.next().split(",");

		Integer[] intArray = new Integer[st.length];

		int i = 0;
		for(String s : st){
			intArray[i++] = Integer.parseInt(s);
		}
		Heapsort heapsort = new Heapsort();
		System.out.print("Before: ");
		for(int k : intArray) {
			System.out.print(k + " ");
		}
		System.out.println("");
		for(int k : intArray) {
			heapsort.add(k);
		}
		// System.out.println("ADD FINISHED");

		List<Integer> list = new ArrayList<Integer>();
		while(true) {
			int j;
			try {
				j = heapsort.remove();
			} catch (NoSuchElementException nsex) {
				break;
			}
			list.add(j);
		}
		System.out.print("After: ");
		for(int k : list) {
			System.out.print(k + " ");
		}
		System.out.println("");
	}

}
// 5,3,8,4,0,2,6,3,8,3,6,43,67


