package org.jse.algorithm;

import java.util.Scanner;

public class QuickSortTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String[]s = sc.next().split(",");
		Integer[] list = new Integer[s.length];
		int i = 0;
		for(String st : s) {
			list[i++] = Integer.parseInt(st);
		}

		System.out.print("Before: ");
		for(int k : list) {
			System.out.print(k + " ");
		}
		new QuickSort().sort(list);

		System.out.println();
		System.out.print("After: ");
		for(int k : list) {
			System.out.print(k + " ");
		}

	}

}
