package org.jse.algorithm;

import java.util.*;
// min heap 
public class Heapsort {

	private LinkedList<Integer> list = new LinkedList<>();

	public void add(int e) {
		list.add(e);
		updateUpwards(list.size() - 1);
	}

	public int remove() {

		int head = list.getFirst();
		if(list.size() > 1) {
			list.set(0, list.removeLast());
			updateDownwards(0);
		} else {
			list.clear();
		}
		return head;
	}

	private void updateUpwards(int index) {

		if(index == 0) {
			return;
		}
		int parent = (index - 1)  / 2;
		if(list.get(parent) > list.get(index)) {
			swap(index, parent);
			updateUpwards(parent);
		}
	}

	private void updateDownwards(int parentIndex) {

		int leftChildIndex = 2*(parentIndex+1) - 1;
		int rightChildIndex = leftChildIndex + 1;
		int swapIndex = -1;
		int parent = list.get(parentIndex);
		if((list.size()-1) >= leftChildIndex ) { // avoid outofbounds
			if(parent > list.get(leftChildIndex)) {
				swapIndex = leftChildIndex;
			}
			if((list.size()-1) >= rightChildIndex ) { // avoid outofbounds
				if(list.get(leftChildIndex) > list.get(rightChildIndex)) {
					swapIndex = rightChildIndex;
				}
			}
		}
		if(swapIndex != -1) {
			// do the swap
			swap(parentIndex, swapIndex);
			updateDownwards(swapIndex);
		}
	}

	private void swap(int a, int b) {
		// System.out.println(a+":"+b+" => before  swap: " + list);
		int tmp = list.get(a);
		list.set(a, list.get(b));
		list.set(b, tmp);
		// System.out.println("after  swap: " + list);
	}

	

	public static void main(String[] args) {
		Integer[] h = new Integer[100];
		List<Integer> list = new ArrayList<Integer>();//Arrays.asList(h);
		System.out.println("h.length = " + h.length);
		System.out.println("list.size() = " + list.size());

	}


}
