package org.jse.algorithm;

public class Bubblesort {


	public <T extends Comparable>
	void sort(T[] t) {
		boolean modified = true;
		while(modified) {
			modified = false;
			for(int i = 0; i < t.length - 1; i++ ) {
				if(t[i].compareTo(t[i+1]) > 0) {
					T tmp = t[i+1];
					t[i+1] = t[i];
					t[i] = tmp;
					modified = true;
				}
			}
		}
	}

}
