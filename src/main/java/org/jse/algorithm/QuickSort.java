package org.jse.algorithm;


/*
 * Based on
 * http://www.youtube.com/watch?v=8hHWpuAPBHo
 * http://www.youtube.com/watch?v=39BV3_DONJc
 * by Sesh Venugopal
 */
public class QuickSort {
	
	
	public <T extends Comparable>
	int split(T[]t, int low, int high) {
		
		T pivot = t[low];
		int leftCursor = low;
		int rightCursor = high;
		
		while(true) {
			while(leftCursor <= rightCursor) {
				if(pivot.compareTo(t[leftCursor]) >= 0) {
					leftCursor++;
				} else {
					break;
				}
			}
			
			while(rightCursor > leftCursor) {
				if(pivot.compareTo(t[rightCursor]) < 0) {
					rightCursor--;
				} else {
					break;
				}
			}
			
			if(leftCursor >= rightCursor) {
				break;
			}
			T tmp = t[leftCursor];
			t[leftCursor] = t[rightCursor];
			t[rightCursor] = tmp;
			leftCursor++;
			rightCursor--;
		}
		
		t[low] = t[leftCursor-1]; 
		t[leftCursor-1] = pivot;
		
		
		return leftCursor-1;
	}
	
	public <T extends Comparable>
	void sort(T[]t, int low, int high) {
		
		if(high - low <= 0) {
			return;
		}
		int pivot = split(t, low, high);
		sort(t, low, pivot - 1);
		sort(t, pivot + 1, high);
		
	}
	
	public <T extends Comparable>
	void sort(T[]t) {
		
		sort(t, 0, t.length-1);
		
	}

}
