package org.jse.lambda;

import java.util.function.Predicate;

public class LambdaExample {
	
	interface Demo {
		public void doSomething();
	}
	
	interface DemoTwo {
		public void doSomething(ObjectOne s);
	}
	
	interface DemoThree {
		public void doSomething(ObjectOne one, ObjectOne two);
	}
	
	
	public void doSomething(Demo demo) {
		demo.doSomething();
	}
	
	public void doSomething(ObjectOne m, DemoTwo demo) {
		demo.doSomething(m);
	}
	
	public void doSomething(ObjectOne m, ObjectOne n, DemoThree demo) {
		demo.doSomething(m,n);
	}
	
	public void doSomething(ObjectTwo m, Predicate<ObjectTwo> pMyObject) {
		if(pMyObject.test(m)) {
			System.out.println("MyObject > 0, x = " + m.getX());
		} else {
			System.out.println("MyObject < 0, x = " + m.getX());
		}
		
	}
	
	public static void main(String[]ar) {
		
		new LambdaExample().doSomething(
				() -> System.out.println("Here comes lambda"));

		new LambdaExample().doSomething(
				new LambdaExample().new ObjectOne(7), m -> System.out.println("square: " + m.getX() * m.getX()));
		
		new LambdaExample().doSomething(
				new LambdaExample().new ObjectOne(8),
				new LambdaExample().new ObjectOne(9),
				(m,n) -> System.out.println("square: " + m.getX() * n.getX()));
		
		Predicate<ObjectTwo> predicateMPredicate = myObject -> myObject.getX() > 10;
		new LambdaExample().doSomething(new LambdaExample().new ObjectTwo(33), predicateMPredicate);
		new LambdaExample().doSomething(new LambdaExample().new ObjectTwo(9), predicateMPredicate);
		
	}

	class ObjectOne {
		int x;
		
		public ObjectOne(int x) {
			this.x = x;
		}
		
		int getX() {
			return this.x;
		}
	}
	
	class ObjectTwo {
		int x;
		
		public ObjectTwo(int x) {
			this.x = x;
		}
		
		int getX() {
			return this.x;
		}
	}
}
