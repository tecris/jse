package org.jse.thread.exception;

public class UncaughtExceptionTest {


	public static void main(String[]s){
		UncaughtExceptionTest.testThread();
	}

	
	public static void testThread(){
		
		Thread.setDefaultUncaughtExceptionHandler(
			new ThreadExceptionHandler());
		
		try {
			new Thread(new MyRunnable()).start();			
		} catch (Exception ex) {

			System.out.println("caught ex: " + ex.getMessage());
		}
	
	}
}
