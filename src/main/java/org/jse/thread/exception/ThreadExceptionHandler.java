package org.jse.thread.exception;

public class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {
	
	public void uncaughtException(Thread t, Throwable e){

		System.out.println(
			"Thread terminated with exception: " + t.getName() + ", exception message:" + e.getMessage());
	}
}
