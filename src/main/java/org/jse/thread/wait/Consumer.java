package org.jse.thread.wait;

import java.util.*;


public class Consumer extends Thread {

	private Server server;

	public Consumer(Server server) {
		this.server = server;
	}

	public void run() {
		while(true){
			List<String> list = this.server.fetch();
			System.out.println(
				Thread.currentThread().getName() 
				+ " - fetch: " + list.size()
				+ " -> " + list);
			
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
