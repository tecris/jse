package org.jse.thread.wait;

import java.util.*;

public class Server {

	private List<String> workList = new ArrayList<>();
	private static final int MAX_SIZE = 2;

	public void store(String mail) {
		synchronized(this.workList) {
			while(workList.size() >= Server.MAX_SIZE) {
				try {
					this.workList.wait();
				} catch(Exception ex) {
					System.out.println("Store, woke up!");
				}
			}
			this.workList.add(mail);
			this.workList.notify();
		}
	}


	public List<String> fetch() {
		synchronized(this.workList) {
			while(this.workList.isEmpty()) {
				try {
					this.workList.wait();
				} catch(Exception ex) {
					System.out.println("Fetch, woke up!");
				}
			}
			List<String> toReturn = new ArrayList<String>();
			toReturn.addAll(this.workList);
			this.workList.clear();
			this.workList.notify();
			return toReturn;
		}
	}

}
