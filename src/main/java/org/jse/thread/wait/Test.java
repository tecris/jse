package org.jse.thread.wait;

public class Test {


	private static int CONSUMERS = 4;
	private static int PRODUCERS = 4;

	public static void main(String[]args){
		

		Server server = new Server();
		if(args.length == 2) {
			Test.CONSUMERS = Integer.parseInt(args[0]);
			Test.PRODUCERS = Integer.parseInt(args[1]);
		} 

		System.out.printf("Starting demo with %d consumers and %d producers\n", 
				Test.CONSUMERS, Test.PRODUCERS );
		
		for(int i=0;i<Test.CONSUMERS;i++){
			new Consumer(server).start();
		}

		for(int i=0;i<Test.PRODUCERS;i++){
			new Producer(server).start();
		}
	}

}
