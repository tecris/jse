package org.jse.thread.wait;

import java.util.*;

public class Producer extends Thread {

	private Server server;

	private static final String[] ALPHABET = 
			"A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".split(" ");

	private static final int SESSION_TOKEN_LENGTH = 10;

	public Producer(Server server) {
		this.server = server;
	}

	public void run() {
		while (true) {
			server.store(this.generateSessionID());

			try {
				Thread.sleep(200);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private String generateSessionID() {
		String s = "";
		for (int i = 0; i < SESSION_TOKEN_LENGTH; i++) {
			s += ALPHABET[Math.abs(new Random().nextInt() % ALPHABET.length)];
		}
		return s;
	}

}
